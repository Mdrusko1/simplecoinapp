package com.simplerecyclerview.activity.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class Coin {

    @PrimaryKey(autoGenerate = true)
    public int id_key;

    @ColumnInfo(name = "id")
    @SerializedName("id")
    public String id;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    public String name;

    @ColumnInfo(name = "symbol")
    @SerializedName("symbol")
    public String symbol;

    @ColumnInfo(name = "rank")
    @SerializedName("rank")
    public String rank;

    @ColumnInfo(name = "price_usd")
    @SerializedName("price_usd")
    public String priceUsd;

    @ColumnInfo(name = "price_btc")
    @SerializedName("price_btc")
    public String priceBtc;

    @ColumnInfo(name = "24h_volume_usd")
    @SerializedName("24h_volume_usd")
    public String $24hVolumeUsd;


    @ColumnInfo(name = "market_cap_usd")
    @SerializedName("market_cap_usd")
    public String marketCapUsd;

    @ColumnInfo(name = "available_supply")
    @SerializedName("available_supply")
    public String availableSupply;

    @ColumnInfo(name = "total_supply")
    @SerializedName("total_supply")
    public String totalSupply;

    @ColumnInfo(name = "max_supply")
    @SerializedName("max_supply")
    public String maxSupply;

    @ColumnInfo(name = "percent_change_1h")
    @SerializedName("percent_change_1h")
    public String percentChange1h;

    @ColumnInfo(name = "percent_change_24h")
    @SerializedName("percent_change_24h")
    public String percentChange24h;

    @ColumnInfo(name = "percent_change_7d")
    @SerializedName("percent_change_7d")
    public String percentChange7d;

    @ColumnInfo(name = "last_updated")
    @SerializedName("last_updated")
    public String lastUpdated;


    public int getId_key() {
        return id_key;
    }

    public void setId_key(int id_key) {
        this.id_key = id_key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    public String getPriceBtc() {
        return priceBtc;
    }

    public void setPriceBtc(String priceBtc) {
        this.priceBtc = priceBtc;
    }

    public String get$24hVolumeUsd() {
        return $24hVolumeUsd;
    }

    public void set$24hVolumeUsd(String $24hVolumeUsd) {
        this.$24hVolumeUsd = $24hVolumeUsd;
    }

    public String getMarketCapUsd() {
        return marketCapUsd;
    }

    public void setMarketCapUsd(String marketCapUsd) {
        this.marketCapUsd = marketCapUsd;
    }

    public String getAvailableSupply() {
        return availableSupply;
    }

    public void setAvailableSupply(String availableSupply) {
        this.availableSupply = availableSupply;
    }

    public String getTotalSupply() {
        return totalSupply;
    }

    public void setTotalSupply(String totalSupply) {
        this.totalSupply = totalSupply;
    }

    public String getMaxSupply() {
        return maxSupply;
    }

    public void setMaxSupply(String maxSupply) {
        this.maxSupply = maxSupply;
    }

    public String getPercentChange1h() {
        return percentChange1h;
    }

    public void setPercentChange1h(String percentChange1h) {
        this.percentChange1h = percentChange1h;
    }

    public String getPercentChange24h() {
        return percentChange24h;
    }

    public void setPercentChange24h(String percentChange24h) {
        this.percentChange24h = percentChange24h;
    }

    public String getPercentChange7d() {
        return percentChange7d;
    }

    public void setPercentChange7d(String percentChange7d) {
        this.percentChange7d = percentChange7d;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
