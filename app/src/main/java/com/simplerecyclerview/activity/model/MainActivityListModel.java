package com.simplerecyclerview.activity.model;

import android.util.Log;

import com.simplerecyclerview.activity.api.ApiClient;
import com.simplerecyclerview.activity.api.ApiInterface;
import com.simplerecyclerview.activity.presenter.MainActivityContract;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityListModel implements MainActivityContract.Model {

    private final String TAG = "MainActivityListModel";

    @Override
    public void getCoinsList(OnFinishedListener onFinishedListener, int pageStart, int pageLimit) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Coin>> call = apiService.getCoins(pageStart, pageLimit);
        call.enqueue(new Callback<List<Coin>>() {
            @Override
            public void onResponse(Call<List<Coin>> call, Response<List<Coin>> response) {
                Log.d(TAG, "Coin data received: " + response.body());
                Log.d(TAG, "Coin data received: " + response.toString());
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<List<Coin>> call, Throwable t) {
                Log.e(TAG, t.toString());
                onFinishedListener.onFailure(t);
            }
        });
    }
}

