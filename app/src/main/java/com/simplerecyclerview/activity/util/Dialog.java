package com.simplerecyclerview.activity.util;

import android.app.AlertDialog;
import android.content.Context;

public class Dialog {

    public static AlertDialog showDialog(Context context, String coinName, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(coinName + " Details");
        builder.setMessage(message);
        builder.setCancelable(true);

        builder.setNeutralButton(
                "OK",
                (dialog, id) -> dialog.cancel());

        return builder.create();
    }
}
