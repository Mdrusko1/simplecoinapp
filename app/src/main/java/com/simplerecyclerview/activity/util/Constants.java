package com.simplerecyclerview.activity.util;

public class Constants {

    public static final String BASE_URL = "https://api.coinmarketcap.com/";
    public static final String BASE_PICTURE_URL = "http://mdrusko1.square7.ch/Pictures/";
    public static final int COIN_ITEMS_LIMIT = 40;

}
