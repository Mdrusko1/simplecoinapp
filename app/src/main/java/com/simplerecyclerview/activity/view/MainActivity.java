package com.simplerecyclerview.activity.view;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.simplerecyclerview.R;
import com.simplerecyclerview.activity.adapter.CoinAdapter;
import com.simplerecyclerview.activity.model.Coin;
import com.simplerecyclerview.activity.presenter.MainActivityContract;
import com.simplerecyclerview.activity.presenter.MainActivityPresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.simplerecyclerview.activity.Application.db;
import static com.simplerecyclerview.activity.util.Constants.COIN_ITEMS_LIMIT;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.txt_search_box)
    EditText txtSearchBox;

    private int pageStart = 0;
    private List<Coin> coinArrayList = new ArrayList<>();
    private CoinAdapter adapter;
    private ProgressDialog pd;
    private MainActivityPresenter mainActivityPresenter;
    private boolean loadAllData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initUI();


        mainActivityPresenter = new MainActivityPresenter(this);
        mainActivityPresenter.requestCoinData(pageStart, COIN_ITEMS_LIMIT);


        // Load more coins
        adapter.setOnBottomReachedListener(position -> {
            pageStart = pageStart + COIN_ITEMS_LIMIT;
            mainActivityPresenter.requestCoinData(pageStart, COIN_ITEMS_LIMIT);
        });


        // Search
        txtSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {
                    adapter.getFilter().filter("");
                } else {
                    adapter.getFilter().filter(s.toString());
                }
            }
        });


        // Save coin to database
        adapter.setOnCoinSave((coin, position) -> {

            // Check if coin exist in database
            AsyncTask.execute(() -> {
                Coin selectedCoin = db.databaseInterface().findById(coin.id);

                runOnUiThread(() -> {
                    if (selectedCoin == null) {

                        // Add coin to database
                        AsyncTask.execute(() -> {
                            try {
                                db.databaseInterface().insertAll(coin);
                                runOnUiThread(() -> {
                                    Toast.makeText(MainActivity.this, "Coin " + coin.name.toUpperCase() + " added to database!", Toast.LENGTH_SHORT).show();
                                });

                            } catch (SQLiteConstraintException ex) {
                                runOnUiThread(() -> Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show());
                            }
                        });

                    } else {
                        Log.i("coin", "Coin " + selectedCoin.name + "exist!");
                        Toast.makeText(MainActivity.this, "Coin " + coin.name.toUpperCase() + " already exist in database.", Toast.LENGTH_SHORT).show();
                    }
                });
            });

        });
    }


    @Override
    public void showProgress() {
        if (pd != null) {
            pd.show();
        } else {
            pd = new ProgressDialog(this);
            pd.setMessage(getString(R.string.loading_coins));
            pd.show();
        }
    }

    @Override
    public void hideProgress() {
        if (pd != null) {
            pd.hide();
        }

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    public void setDataToViews(List<Coin> coinList) {

        pageStart = pageStart + COIN_ITEMS_LIMIT;

        if (coinList != null) {
            coinArrayList.addAll(coinList);
        } else {
            Toast.makeText(MainActivity.this, getString(R.string.error_getting_data), Toast.LENGTH_SHORT).show();
        }

        adapter.setCoinList(coinArrayList);
        hideProgress();
    }


    @Override
    public void onResponseFailure(Throwable throwable) {
        hideProgress();
        Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        Log.d("Error", throwable.getMessage());
    }


    @Override
    public void initUI() {
        adapter = new CoinAdapter(this, coinArrayList);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.app_name));
    }

    @Override
    public void onRefresh() {
        coinArrayList.clear();
        pageStart = 0;
        mainActivityPresenter.requestCoinData(pageStart, COIN_ITEMS_LIMIT);
    }


}
