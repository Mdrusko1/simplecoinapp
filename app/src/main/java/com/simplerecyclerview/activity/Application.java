package com.simplerecyclerview.activity;

import android.arch.persistence.room.Room;

import com.simplerecyclerview.activity.database.AppDatabase;


public class Application extends android.app.Application {

    public static AppDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();

        // Database
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database").build();

    }
}
