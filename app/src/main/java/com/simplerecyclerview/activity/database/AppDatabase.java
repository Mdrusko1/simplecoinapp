package com.simplerecyclerview.activity.database;


import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.SkipQueryVerification;

import com.simplerecyclerview.activity.dao.LocalDatabaseInterface;
import com.simplerecyclerview.activity.model.Coin;

@Database(entities = {Coin.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract LocalDatabaseInterface databaseInterface();

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

}
