package com.simplerecyclerview.activity.presenter;

import com.simplerecyclerview.activity.model.Coin;

import java.util.List;

public interface MainActivityContract {

    interface Model {

        interface OnFinishedListener {
            void onFinished(List<Coin> coinList);

            void onFailure(Throwable t);
        }

        void getCoinsList(OnFinishedListener onFinishedListener, int pageStart, int pageLimit);

    }

    interface View {

        void initUI();

        void showProgress();

        void hideProgress();

        void setDataToViews(List<Coin> coinList);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestCoinData(int pageStart, int pageLimit);
    }
}
