package com.simplerecyclerview.activity.presenter;

import com.simplerecyclerview.activity.model.Coin;
import com.simplerecyclerview.activity.model.MainActivityListModel;

import java.util.List;
import java.util.Objects;

public class MainActivityPresenter implements MainActivityContract.Presenter, MainActivityContract.Model.OnFinishedListener {

    private MainActivityContract.View mainActivityView;
    private MainActivityContract.Model mainActivityModel;


    public MainActivityPresenter(MainActivityContract.View mainActivityView) {
        this.mainActivityView = mainActivityView;
        mainActivityModel = new MainActivityListModel();
    }

    @Override
    public void onFinished(List<Coin> coinList) {
        if (mainActivityView != null) {
            mainActivityView.hideProgress();
        }
        Objects.requireNonNull(mainActivityView).setDataToViews(coinList);
    }

    @Override
    public void onFailure(Throwable t) {
        if (mainActivityView != null) {
            mainActivityView.hideProgress();
        }
        Objects.requireNonNull(mainActivityView).onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        mainActivityView = null;
    }

    @Override
    public void requestCoinData(int pageStart, int pageLimit) {
        if (mainActivityView != null) {
            mainActivityView.showProgress();
        }
        mainActivityModel.getCoinsList(this, pageStart, pageLimit);
    }

}
