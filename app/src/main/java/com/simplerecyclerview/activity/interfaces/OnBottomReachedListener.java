package com.simplerecyclerview.activity.interfaces;

public interface OnBottomReachedListener {
    void onBottomReached(int position);
}
