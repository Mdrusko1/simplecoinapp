package com.simplerecyclerview.activity.filter;

import android.widget.Filter;

import com.simplerecyclerview.activity.adapter.CoinAdapter;
import com.simplerecyclerview.activity.model.Coin;

import java.util.ArrayList;
import java.util.List;

public class CustomFilter extends Filter {

    private CoinAdapter adapter;
    private List<Coin> filterList;


    public CustomFilter(List<Coin> filterList, CoinAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;

    }

    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();

        //CHECK CONSTRAINT VALIDITY
        if (constraint != null && constraint.length() > 0) {
            //CHANGE TO UPPER
            constraint = constraint.toString().toUpperCase();
            //STORE OUR FILTERED COINS
            ArrayList<Coin> filterItem = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                //CHECK
                if (filterList.get(i).getName().toUpperCase().contains(constraint)) {
                    //ADD COIN TO FILTERED COINS
                    filterItem.add(filterList.get(i));
                }
            }

            results.count = filterItem.size();
            results.values = filterItem;
        } else {
            results.count = filterList.size();
            results.values = filterList;

        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.setCoinList((List<Coin>) results.values);

        //REFRESH
        adapter.notifyDataSetChanged();
    }
}