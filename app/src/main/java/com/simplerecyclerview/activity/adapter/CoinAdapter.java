package com.simplerecyclerview.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Camera;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.simplerecyclerview.R;
import com.simplerecyclerview.activity.dao.OnCoinSave;
import com.simplerecyclerview.activity.filter.CustomFilter;
import com.simplerecyclerview.activity.interfaces.OnBottomReachedListener;
import com.simplerecyclerview.activity.model.Coin;
import com.simplerecyclerview.activity.util.Dialog;
import com.simplerecyclerview.activity.util.Numbers;
import com.simplerecyclerview.activity.util.PicassoTrustAll;
import com.simplerecyclerview.activity.view.CameraActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.simplerecyclerview.activity.util.Constants.BASE_PICTURE_URL;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.MyViewHolder> implements Filterable {

    private List<Coin> coinList, filterList;
    private Context context;
    private OnBottomReachedListener onBottomReachedListener;
    private OnCoinSave onCoinSave;
    private CustomFilter filter;

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name)
        TextView txtName;
        @BindView(R.id.txt_details)
        TextView txtDetails;
        @BindView(R.id.img_logo)
        ImageView imgLogo;

        private MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setOnCoinSave(OnCoinSave onCoinSave) {
        this.onCoinSave = onCoinSave;
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public void setCoinList(List<Coin> coinList) {
        this.coinList = coinList;
        notifyDataSetChanged();
    }

    public CoinAdapter(Context context, List<Coin> coinList) {
        this.context = context;
        this.coinList = coinList;
        this.filterList = coinList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Coin coin = coinList.get(position);
        holder.txtName.setText(coin.name);
        holder.txtDetails.setText("$ " + Numbers.convertDecimal(Double.parseDouble(coin.priceUsd)));


        try {
            PicassoTrustAll.getInstance(context).load(BASE_PICTURE_URL + coinList.get(position).id + ".png").into(holder.imgLogo);
        } catch (Exception e) {
            PicassoTrustAll.getInstance(context).load(R.drawable.ic_no_image).into(holder.imgLogo);
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String coinDetails = "Name: " + coin.name + "\n" +
                        "Price $" + Numbers.convertDecimal(Double.parseDouble(coin.priceUsd)) + "\n" +
                        "Rank: " + coin.rank + "\n" +
                        "Symbol: " + coin.symbol + "\n" +
                        "Total supply: " + Numbers.convertDecimal(Double.parseDouble(coin.totalSupply)) + "\n" +
                        "Price BTC: " + Numbers.convertDecimal(Double.parseDouble(coin.priceBtc)) + "\n" +
                        "24h Volume Usd " + Numbers.convertDecimal(Double.parseDouble(coin.$24hVolumeUsd)) + "\n" +
                        "Market Cap USD: " + Numbers.convertDecimal(Double.parseDouble(coin.marketCapUsd)) + "\n" +
                        "Available Supply: " + Numbers.convertDecimal(Double.parseDouble(coin.availableSupply)) + "\n" +
                        "Last updated: " + Numbers.convertTime(Long.parseLong(coin.lastUpdated));


                Dialog.showDialog(context, coin.name, coinDetails).show();

            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onCoinSave.OnCoinSaveClicked(coin, position);

                context.startActivity(new Intent(context, CameraActivity.class));


                return true;
            }

        });


        if (position == coinList.size() - 1) {
            onBottomReachedListener.onBottomReached(position);
        }
    }

    @Override
    public int getItemCount() {
        return coinList.size();
    }

    //RETURN FILTER OBJ
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilter((List<Coin>) filterList, this);
        }

        return filter;
    }


}
