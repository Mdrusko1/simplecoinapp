package com.simplerecyclerview.activity.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.simplerecyclerview.activity.model.Coin;

import java.util.List;

@Dao
public interface LocalDatabaseInterface {

    @Query("SELECT * FROM Coin")
    List<Coin> getCoins();

    @Insert
    void insertAll(Coin... coins);

    @Delete
    void deleteItem(Coin... coins);


    @Query("SELECT * FROM Coin WHERE id LIKE :id ")
    Coin findById(String id);

}
