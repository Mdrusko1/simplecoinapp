package com.simplerecyclerview.activity.dao;

import com.simplerecyclerview.activity.model.Coin;

public interface OnCoinSave {

    void OnCoinSaveClicked(Coin coin, int position);
}
